
def ClearRegLine(r):
    return "(W) mov (8|M0) r{A}<1>:ud   0:ud \n".format(A=r)

def AsmInstGeneral(op,simd,dest,src0,src1=None):
    raise Exception("Unimplemented.")

def AsmInstGeneralGRFLine(op,simd,dest,src0,src1=None):
    def format_src(src,simd):
        if isinstance(src,int):
            c=1
            a=b=simd
            if simd>8:
                a=b=8
            elif simd==1:
                a=c=0
            return "r{s}.0<{a};{b},{c}>:ud".format(s=src,a=a,b=b,c=c)
        elif isinstance(src,str) :
            return src
        elif src is None:
            return ""
        else:
            raise Exception("Unknown inst type.")

    inst="(W) {op:<5} ({simd}|M0) r{dest}<1>:ud   {src0}  {src1} \n"
    src0=format_src(src0,simd)
    src1=format_src(src1,simd)

    return inst.format(op=op,simd=simd,dest=dest,src0=src0,src1=src1)

def RegLineOperation(op,dest,src0,src1=None,nline=1):
    if nline not in (1,2):
        raise Exception("Invalid value : nline needs to be 1 or 2.")
    return AsmInstGeneralGRFLine(op,nline*8,dest,src0,src1)

def SubLineOperation(op,dest,src0,src1=None,n=1):
    if n<1 or n>8:
        raise Exception("Invalid value : n needs to be in [1,8].")
    return AsmInstGeneralGRFLine(op,n,dest,src0,src1)

def OrsumOverRegRange(r_dest,r_start,r_end,
            perserve_src=False,need_clear_dest=True,buf=None):
    if not perserve_src:
        return OrsumOverRegRangeTreeStyle(r_dest,r_start,r_end)
 
    if buf and buf[0]-buf[1] > 4:
        return OrsumOverRegRangeWithBuffer(r_dest,r_start,r_end,buf[0],buf[1])

    if need_clear_dest:
        ret="(W) mov (8|M0) r{dest}<1>:ud   0:ud \n".format(dest=r_dest)
    else:
        ret=""

    for r in range(r_start,r_end+1):
        ret+=RegLineOperation("or",r_dest,r_dest,r,nline=1)

    return ret


def OrsumOverRegRangeWithBuffer(r_dest,r_start,r_end,buf_start,buf_end):
    raise Exception("Unimplemented.")
    ret=""

    range_set_next=list(range(r_start,r_end+1))

    while(len(range_set_next)>2):
        range_set=range_set_next
        range_set_next=[]
        for i in range(0, len(range_set), 2):
            if i+1<len(range_set):
                ret+=RegLineOperation("or",range_set[i],range_set[i],range_set[i+1],nline=1)
            range_set_next.append(range_set[i])

    if len(range_set_next)==1 and r_dest != range_set_next[0]:
        ret+=AsmInstGeneralGRFLine("mov",8,r_dest,range_set_next[0])
    elif len(range_set_next)==2:
        ret+=AsmInstGeneralGRFLine("or",8,r_dest,range_set_next[0],range_set_next[1])
    else:
        raise Exception("Unexpected.")

    return ret

def OrsumOverRegRangeTreeStyle(r_dest,r_start,r_end):

    ret=""

    range_set_next=list(range(r_start,r_end+1))

    while(len(range_set_next)>2):
        range_set=range_set_next
        range_set_next=[]
        for i in range(0, len(range_set), 2):
            if i+1<len(range_set):
                ret+=RegLineOperation("or",range_set[i],range_set[i],range_set[i+1],nline=1)
            range_set_next.append(range_set[i])

    if len(range_set_next)==1 and r_dest != range_set_next[0]:
        ret+=AsmInstGeneralGRFLine("mov",8,r_dest,range_set_next[0])
    elif len(range_set_next)==2:
        ret+=AsmInstGeneralGRFLine("or",8,r_dest,range_set_next[0],range_set_next[1])
    else:
        raise Exception("Unexpected.")

    return ret

def MergeReg(r,operator):
    ret="""
(W) {op:<5} (4|M0) r{A}<1>:ud   r{A}.0<4;4,1>:ud  r{A}.4<4;4,1>:ud
(W) {op:<5} (2|M0) r{A}<1>:ud   r{A}.0<2;2,1>:ud  r{A}.2<2;2,1>:ud
(W) {op:<5} (1|M0) r{A}<1>:ud   r{A}.0<0;1,0>:ud  r{A}.1<0;1,0>:ud
""".format(op=operator,A=r)
    return ret

def MergeRegsToHead(r_start,r_end,operator):
    ret="\n"
    for r in range(r_start,r_end+1):
        ret+="(W) {op:<5} (4|M0) r{A}<1>:ud   r{A}.0<4;4,1>:ud  r{A}.4<4;4,1>:ud \n".format(
            op=operator,A=r
        )
    for r in range(r_start,r_end+1):
        ret+="(W) {op:<5} (2|M0) r{A}<1>:ud   r{A}.0<2;2,1>:ud  r{A}.2<2;2,1>:ud \n".format(
            op=operator,A=r
        )
    for r in range(r_start,r_end+1):
        ret+="(W) {op:<5} (1|M0) r{A}<1>:ud   r{A}.0<0;1,0>:ud  r{A}.1<0;1,0>:ud \n".format(
            op=operator,A=r
        )
    return ret

def StoreRegLine(r,surf,r_addr):
    raise Exception("Unimplemented.")
    

def Store2RegLines(r,surf,r_addr):
    tmplt="(W) sends (16|M0) null:w     r{ptr}   r{A}   0x8C   {desc:#x}  //- st.us.r \n"
    return tmplt.format(ptr=r_addr,A=r,desc=(0x4025E00+surf))


def StoreMultipleRegLines(r_start,r_end,surf,r_addr):
    num_r=r_end-r_start+1
    ret=""
    if num_r==1:
        ret+=StoreRegLine(r_start,surf,r_addr)
    elif num_r >=2:
        for r in range(r_start,r_end,2):
            ret+=Store2RegLines(r,surf,r_addr)
            ret+=RegLineOperation("add",r_addr,r_addr,"0x40:ud",nline=2)
        if num_r % 2 != 0:
            raise Exception("Unimplemented. Please use even num of regs.")

    else:
        raise Exception("Invalid value : r_start,r_end.")
    
    return ret



def StoreRegHead(r,num_regs,surf,r_addr):
    if surf>=255:
        raise Exception("surf id out of range.")
    if num_regs<=0 or num_regs>4:
        raise Exception("num_regs out of range.")

    rgba= ((0xf << num_regs) & 0xf ) << 8

    return "(W) sends (1|M0) null:w  r{addr}  r{outreg}  {ex_desc:#x} {desc:#x} // st.us.[rgba]:l(rgba) = {l}; msg_len = 1;".format(
        addr=r_addr,outreg=r,
        ex_desc=0xC + (num_regs<<6),
        desc=(0x2026000+surf + rgba),
        l=num_regs)

def DivdingLine():
    return "//////////////////////////////\n"