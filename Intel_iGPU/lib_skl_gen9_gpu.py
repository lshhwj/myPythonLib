
import threading, queue
import re
import subprocess as sp
from hyutil import easy_subprocess as easy_sp

nSlices=1
nSubslices=3
EU_ID_LIST=[0b000,0b001,0b010,0b011,0b1000,0b1001,0b1010,0b1011]
nEUsInSubslice=len(EU_ID_LIST)
nThreadsInEU=7

class EUThread(object):
    def __init__(self):
        self.GRF=[]
        self.ARF=[]

    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        return False

class EU(dict):
    def __init__(self):
        for i in range(0,nThreadsInEU):
            self.update({i:EUThread()})

class Subslice(dict):
    def __init__(self):
        for id in EU_ID_LIST:
            self.update({id:EU()})

class GPUTop(dict):
    def __init__(self):
        for i in range(0,nSubslices):
            self.update({i:Subslice()})


class hyHammingDistSrv(object):
    __STATE_MASK_LOADING=1
    __STATE_VAL_LOADING=2

    def __init__(self,src_file):
        self.src=GPUTop()
        with open(src_file,'r') as inf:
            state = 0
            subsliceid, euid, threadid = 0,0,0
            for line in inf:
                if line.startswith("[Mask][Thread]"):
                    state = hyHammingDistSrv.__STATE_MASK_LOADING
                    subsliceid, euid, threadid = self._get_hw_ids(line)
                    self.src[subsliceid][euid][threadid].Mask=[]
                    if "(starting from r10)" in line:
                        self.src[subsliceid][euid][threadid].reg_offset=80
                    else:
                        self.src[subsliceid][euid][threadid].reg_offset=0
                    # self.src[subslice][euid][threadid].Mask_bitcnt=[]

                elif line.startswith("[Init][Thread]"):
                    state = hyHammingDistSrv.__STATE_VAL_LOADING
                    subsliceid, euid, threadid = self._get_hw_ids(line)

                elif state == hyHammingDistSrv.__STATE_MASK_LOADING:
                    vals=filter(None,line.strip().split())
                    self.src[subsliceid][euid][threadid].Mask.extend(vals)
                
                elif state == hyHammingDistSrv.__STATE_VAL_LOADING:
                    vals=filter(None,line.strip().split())
                    self.src[subsliceid][euid][threadid].GRF.extend(vals)

        for subsliceid,subslice in self.src.items():
            for euid,eu in subslice.items():
                for tid, thread in eu.items():
                    thread.GRF=[int(val,16) for val in thread.GRF]
                    thread.Mask=[int(val,16) for val in thread.Mask]
                    thread.Mask_bitcnt=[bin(val).count("1") for val in thread.Mask]
                    thread.Mask_total_bits=sum(thread.Mask_bitcnt)

    def _get_hw_ids(self,line):
        fields = line.split(" ")
        return int(fields[1]), int(fields[2]), int(fields[3])

    def compare(self,grf,info=None,compare_range=(80,None)):
        if info is None:
            info=basic_info(grf)

        subsliceid, euid, threadid = info["subsliceID"], info["EUID"], info["threadID"]
        compare_range=(compare_range[0],compare_range[1] if compare_range[1] else len(grf))

        need_calc_total_bits=False
        total_mismatch=0
        total_bits=0
        with self.src[subsliceid][euid][threadid] as thread:
            if compare_range[0] < thread.reg_offset:
                AssertionError("compare_range[0] < thread.reg_offset")
            if compare_range[1]-thread.reg_offset > len(thread.GRF) or compare_range[1]>len(grf):
                AssertionError("compare_range[1] too large.")
            
            if (compare_range[0],compare_range[1]) == (thread.reg_offset,len(grf)) :
                total_bits = thread.Mask_total_bits
                need_calc_total_bits = True

            for i in range(compare_range[0],compare_range[1]):
                ref = i - thread.reg_offset
                total_mismatch+=bin(thread.GRF[ref] ^ ( int(grf[i],16) & thread.Mask[ref] ) ).count("1")
                if need_calc_total_bits:
                    total_bits+=thread.Mask_bitcnt[ref]
            

        return total_mismatch, total_bits


class ReadingService(threading.Thread):

    def __init__(self,inbuf,outbuf):
        threading.Thread.__init__(self)
        self.inbuf=inbuf
        self.outbuf=outbuf

    def run(self):
        if not self.inbuf or not self.outbuf:
            return
        while True:
            batch=extract_one_batch(self.inbuf)
            if batch:
                self.outbuf.put(batch)
            else:
                break

class CollectService(object):
    def __init__(self):
        self.cmd_line=""
        self.ocl_sp=None
        self.ocl_ReadingService=None
        self.q=queue.Queue()
    
    def start(self,batch=1,block=False):
        if self.ocl_ReadingService and self.ocl_ReadingService.is_alive():
            raise Exception("OCL process still running.")
        self.cmd_line= \
            "{ocl_shell} build/dump.r0.EUIDTIDtm0.r0.r3-r127.asm {NumThreads} -s {SPACE} -E 16 --no-compact --batch {batch}".format(
            ocl_shell="../ocl_run_genasm/main",
            NumThreads=nSlices*nSubslices*nEUsInSubslice*nThreadsInEU,
            SPACE=nSlices*nSubslices*nEUsInSubslice*nThreadsInEU*1024,
            batch=batch
        )
        self.ocl_sp=easy_sp.Popen(self.cmd_line,stdout=sp.PIPE)

        self.ocl_ReadingService=ReadingService(inbuf=self.ocl_sp.stdout,outbuf=self.q)
        self.ocl_ReadingService.start()

        self.check()

        if block:
            self.ocl_ReadingService.join()
    
    def cleanup(self):
        if self.ocl_sp:
            self.ocl_sp.wait()
        if self.ocl_ReadingService:
            self.ocl_ReadingService.join()
        while not self.q.empty():
            self.q.get()
        self.ocl_sp=None
        self.ocl_ReadingService=None
    
    def check(self):
        if self.ocl_sp is None:
            raise Exception("Unexpected: self.ocl_sp is None")
        elif self.ocl_sp.poll() is not None:
            raise Exception("Unexpected: OpenCL Shell process exited early.")


    def get(self):
        while self.ocl_ReadingService and self.ocl_ReadingService.is_alive():
            try:
                d= self.q.get(timeout=0.3)
            except queue.Empty:
                d= None
            if d:
                return d
        if not self.q.empty():
            return self.q.get()
        else:
            return None

    def __iter__(self):
        return self

    def __next__(self):
        d=self.get()
        if d:
            return d
        else:
            raise StopIteration


def extract_one_batch(stream):

    line="-"
    while (not line.startswith("surf[0]:")) and line != "":
        line=stream.readline()
        if line == "":
            return []

    threads=[]
    curr=[]

    for line in stream:
        if not line:
            break

        if(line.startswith('[')) or re.match("^\s*\n",line):
            break
        
        addr=int(line.split(":")[0].strip())
    
        if addr % 1024 == 0 and addr != 0:
            threads.append(curr)
            curr=[]
        
        curr.extend(filter(None,line.split(":")[1].strip().split(" ")))

    if curr:
        threads.append(curr)

    return threads


def basic_info(grf_data_str):
    if not grf_data_str or len(grf_data_str) != (8*128):
        raise Exception("Invalid input="+str(grf_data_str))

    IDs_field=int(grf_data_str[8+5],16)
    return {
        "glbX" :int(grf_data_str[1],16),
        "glbY":int(grf_data_str[6],16),
        "glbZ":int(grf_data_str[7],16),
        "threadID":IDs_field & 0x7,
        "EUID": ( IDs_field >>8 ) & 0xf,
        "subsliceID": ( (IDs_field >>8 ) >>4) & 0x3,
        "sliceID": ( IDs_field >>8 >>4 >>2 ) & 0x3,
        "start_tm0" : (int(grf_data_str[15],16)<<32) + int(grf_data_str[14],16)
    }

def calc_hamming_distance(grf_data_str,grf_):
    if not grf_data_str:
        return False
