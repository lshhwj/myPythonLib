from ropgadget.loaders.elf import *
from ropgadget.loaders.pe import *
from ropgadget.loaders.raw import *
from ropgadget.loaders.macho import *
from ropgadget.loaders.universal import *
from binascii import unhexlify


class Binary(object):
    def __init__(self, filepath):
        self.__fileName = filepath
        self.__rawBinary = None
        self.__binary = None
        self.errno = 0
        try:
            fd = open(self.__fileName, "rb")
            self.__rawBinary = fd.read()
            fd.close()
        except:
            # print("[Error] Can't open the binary or binary not found")
            self.errno = 1
            return

        if self.__rawBinary[:4] == unhexlify(b"7f454c46"):
            self.__binary = ELF(self.__rawBinary)
        # elif self.__rawBinary[:2] == unhexlify(b"4d5a"):
        #      self.__binary = PE(self.__rawBinary)
        # elif self.__rawBinary[:4] == unhexlify(b"cafebabe"):
        #      self.__binary = UNIVERSAL(self.__rawBinary)
        # elif self.__rawBinary[:4] == unhexlify(b"cefaedfe") or self.__rawBinary[:4] == unhexlify(b"cffaedfe"):
        #      self.__binary = MACHO(self.__rawBinary)
        else:
            # print("[Error] Binary format not supported")
            self.errno = 2
            return

    def getFileName(self):
        return self.__fileName

    def getRawBinary(self):
        return self.__rawBinary

    def getBinary(self):
        return self.__binary

    def getEntryPoint(self):
        return self.__binary.getEntryPoint()

    def getDataSections(self):
        return self.__binary.getDataSections()

    def getExecSections(self):
        return self.__binary.getExecSections()

    def getArch(self):
        return self.__binary.getArch()

    def getArchMode(self):
        return self.__binary.getArchMode()

    def getFormat(self):
        return self.__binary.getFormat()

    def good(self):
        return not self.errno

    def getExecSectionsIter(self):
        for section in self.__binary._ELF__shdr_l:
            if (section.sh_flags & 0x4) and (section.sh_flags & 0x2):
                yield {
                    "name": section.str_name,
                    "offset": section.sh_offset,
                    "size": section.sh_size,
                    "vaddr": section.sh_addr,
                    "opcodes": bytes(self.__binary._ELF__binary[section.sh_offset:section.sh_offset + section.sh_size])
                }

    def getExecSections(self):
        ret = []
        for section in self.__binary._ELF__shdr_l:
            if (section.sh_flags & 0x4) and (section.sh_flags & 0x2):
                ret += [{
                    "name": section.str_name,
                    "offset": section.sh_offset,
                    "size": section.sh_size,
                    "vaddr": section.sh_addr,
                    "opcodes": bytes(self.__binary._ELF__binary[section.sh_offset:section.sh_offset + section.sh_size])
                }]
        return ret

    def getExecSectionVAddrRange(self):
        try:
            min_vaddr = min(section.sh_addr
                            for section in self.__binary._ELF__shdr_l if
                            (section.sh_flags & 0x4) and (section.sh_flags & 0x2))
        except (ValueError, TypeError) as err:
            print("[WARN] Binary::getExecSectionVAddrRange: ", repr(err))
            min_vaddr = 0

        try:
            max_vaddr = max(section.sh_addr + section.sh_size
                            for section in self.__binary._ELF__shdr_l if
                            (section.sh_flags & 0x4) and (section.sh_flags & 0x2))
        except (ValueError, TypeError) as err:
            print("[WARN] Binary::getExecSectionVAddrRange: ", repr(err))
            max_vaddr = 0x3FFFFFFFFFFFFFFFF

        return (min_vaddr, max_vaddr)

    def __str__(self):
        try:
            entry = self.getEntryPoint()
        except:
            entry = "None"
        Sect = self.getSections()

        return (
            "===================================\n"
            "[Binary] FileName={filename}\n"
            "Arch= {fmt}-{arch:<5}\tEntry= {entry:<5}"
        ).format(
            filename=self.getFileName(),
            fmt=self.getFormat(),
            arch="64" if self.getArchMode() == CS_MODE_64 else "32",
            entry=entry
        )

    def str_detailed(self):
        try:
            entry = self.getEntryPoint()
        except:
            entry = "None"
        Exec = self.getExecSections()
        Sect = self.getSections()

        return (
            "===================================\n"
            "[Binary] FileName={filename}\n"
            "Arch= {fmt}-{arch:<5}\tEntry= {entry:<5}\n"
            "[Exec] ======================\n{execlist}\n"
            "[Sections] ==================\n{datalist}"
        ).format(
            filename=self.getFileName(),
            fmt=self.getFormat(),
            arch="64" if self.getArchMode() == CS_MODE_64 else "32",
            entry=entry,
            execlist="\n".join("{offset:<#8x}: {vaddr:<#8x} ~ {endvaddr:<#8x} {size:>8} Byte".format(
                offset=e.get("offset"), vaddr=e.get("vaddr"), endvaddr=e.get("vaddr") + e.get("size") - 1,
                size=e.get("size"))
                               for e in Exec),
            datalist="\n".join("{name:<18} @{offset:<#8x} : {vaddr:<#8x} ~ {endvaddr:<#8x} {size:>8} Byte".format(
                name=e.get("name"), offset=e.get("offset"), endvaddr=e.get("vaddr") + e.get("size") - 1,
                vaddr=e.get("vaddr"), size=e.get("size"))
                               for e in Sect)
        )
