
import statistics

def report_basic(datalist):
    if datalist is None or len(datalist)<2:
        return ""
    return "min={:.3f}; max={:.3f}; mean={:.3f}; std={:.3f};".format(
            min(datalist),
            max(datalist),
            statistics.mean(datalist),
            statistics.stdev(datalist)
        )