import sys

def eprint(*args,**kargs):
    print(*args, **kargs,file=sys.stderr)

def WaitEnterConfirm(msg=""):
    if msg:
        print(msg)
    i=input("Press N to stop; press any key to continue...")
    return i not in ['n','N',"no","NO","No"]

def pickle_load(fname):
    import pickle
    with open(fname,"rb") as inf:
        ret=pickle.load(inf)
    return ret

def pickle_dump(fname,data):
    import pickle
    with open(fname,"wb") as outf:
        pickle.dump(data,outf,protocol=4)

def pickle_file(fname,*,dump=None):
    import pickle
    if dump is None:
        with open(fname,"rb") as inf:
            dump=pickle.load(inf)
    else:
        with open(fname,"wb") as outf:
            pickle.dump(dump,outf,protocol=4)
    return dump

def json_file(fname,*,dump=None):
    import json
    if dump is None:
        with open(fname,"r") as inf:
            dump=json.load(inf)
    else:
        with open(fname,"w") as outf:
            json.dump(dump,outf)
    return dump

class Unbuffered(object):
   def __init__(self, stream):
       self.stream = stream
   def write(self, data):
       self.stream.write(data)
       self.stream.flush()
   def writelines(self, datas):
       self.stream.writelines(datas)
       self.stream.flush()
   def __getattr__(self, attr):
       return getattr(self.stream, attr)