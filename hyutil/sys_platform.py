import sys
from functools import lru_cache

@lru_cache(None)
def get_screen_resolution_px():
    def gtk_method():
        from gi.repository import Gtk
        s = Gtk.Window().get_screen()
        m = s.get_monitor_at_window(s.get_active_window())
        monitor = s.get_monitor_geometry(m)
        return monitor.width, monitor.height

    def xrandr_method():
        import subprocess
        proc = subprocess.Popen('xrandr | grep "\*" | cut -d" " -f4',shell=True, stdout=subprocess.PIPE)
        output = proc.communicate()[0]
        if proc.wait()>0:
            raise RuntimeError("xrandr failed.")
        resolution = output.split()[0].split(b'x')
        return resolution[0], resolution[1]

    def win32api_method():
        from win32api import GetSystemMetrics
        return GetSystemMetrics(0), GetSystemMetrics(1)

    width=height=0
    methods=[xrandr_method, gtk_method, win32api_method]
    for func in methods:
        try:
            width, height = func()
        except:
            width=height=0
        else:
            break
            
    return int(width), int(height)