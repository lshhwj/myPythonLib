import plotly.offline as offplt
import plotly.graph_objs as pltgo
from .sys_platform import get_screen_resolution_px

def offline_show(plot_traces,screen_height_ratio=0.6):
    layout = pltgo.Layout(
        height=max(400,float(get_screen_resolution_px()[1])*(screen_height_ratio-0.1)),
        margin=dict(
            l=0,
            r=10,
            b=0,
            t=100
        )
    )
    fig=pltgo.Figure(data=plot_traces,layout=layout)
    offplt.plot(fig, auto_open=True)
    return fig