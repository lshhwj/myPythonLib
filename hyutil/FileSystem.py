import os

def getAllFilesInDir(dir):
    """Get all the files in the dir and subdirs"""
    allfiles = []
    for root, _, files in os.walk(dir,topdown=True,followlinks=True):
        for file in files:
            filepath = os.path.join(root, file)
            allfiles += [filepath]
    return allfiles

def isFileExist(path):
    return os.path.exists(path)
