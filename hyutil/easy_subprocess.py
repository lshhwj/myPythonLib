import subprocess as sp
import shlex
from functools import lru_cache
import queue,threading


@lru_cache(None)
def nCPUs():
    import os
    return len(os.sched_getaffinity(0))

@lru_cache(None)
def calc_parallel(parallel,limit=None):
    parallel= (parallel if isinstance(parallel,int) else (nCPUs() if parallel==True else 1))
    parallel= (parallel if parallel>=0 else max(1,nCPUs()+parallel+1) )
    if limit is not None:
        parallel= min(parallel,limit)
    return parallel

@lru_cache(None)
def calc_dispatch(dispatch,n_jobs : int):
    if dispatch is None or dispatch=="all":
        return None
    if isinstance(dispatch,int):
        return calc_parallel(dispatch)
    if isinstance(dispatch,str):
        dispatch=dispatch[:-len("n_jobs")] if dispatch.endswith("n_jobs") else dispatch
        dispatch=dispatch[:-len("parallel")] if dispatch.endswith("parallel") else dispatch
        coef =float(dispatch[:-1]) if dispatch.endswith("*") else 1.0
        A    =  int(dispatch[:-1]) if dispatch.endswith("+") else 0
        return max(1,A+int(coef*float(n_jobs)))

    raise ValueError("Wrong dispatch variable type="+str(type(dispatch)))

class Popen(sp.Popen):
    """All args are passed directly to subprocess.Popen() except cmd. If cmd is a
    string and shell=False (default), it will be sent through shlex.split() prior
    to being sent to subprocess.Popen as *args.
    The only other difference is that this defualts universal_newlines to True
    (streams yield Python strings instead of bytes).
    """
    __slots__ = ()

    def __init__(self, cmd, input=None, stdin=None,
                 universal_newlines=True, shell=False, **kwargs):
        if input is not None:
            if stdin is not None:
                raise ValueError(
                    'stdin and input arguments may not both be used.')
            stdin = sp.PIPE

        if isinstance(cmd, str) and not shell:
            cmd = shlex.split(cmd)

        super().__init__(cmd, stdin=stdin, universal_newlines=universal_newlines,
                         shell=shell, **kwargs)
        if input:
            if isinstance(input, str):
                self._stdin_write(input)
            else:
                raise ValueError(
                    'input should be string.')
            # else:
            #     for i in input:
            #         self.stdin.writelines(i, '\n')
            # self.stdin.close()

def check_output(cmd, input=None, stdin=None,
                 universal_newlines=True, shell=False, **kwargs):
    stdout,_ = Popen(cmd, input=input, stdin=stdin, stdout=sp.PIPE, universal_newlines=universal_newlines,
                         shell=shell, **kwargs).communicate()
    return stdout

def pipe(*commands, input=None,
         stdin=None, stderr=None, **kwargs):
    """like the run() function, but will take a list of commands and pipe them
    into each other, one after another. If pressent, the 'stderr' parameter
    will be passed to all commands. Either 'input' or 'stdin' will be passed to
    the initial command all other **kwargs will be passed to the final command.
    If grab_it=True, stdout will be returned as a ProcOutput instance.
    """
    if len(commands)<=1:
        raise ValueError(
            'Too less commands.')
    prevout = Popen(commands[0], input=input,
                stdin=stdin, stdout=sp.PIPE, stderr=stderr).stdout
    for cmd in commands[1:-1]:
        prevout = Popen(cmd, stdin=prevout, stdout=sp.PIPE, stderr=stderr).stdout

    return Popen(commands[-1], stdin=prevout, stderr=stderr, **kwargs)

    # if grab_it:
    #     return grab(commands[-1], stdin=out, stderr=stderr, **kwargs)
    # else:
    #     return run(commands[-1], stdin=out, stderr=stderr, **kwargs)

from concurrent import futures
from concurrent.futures import as_completed

class Scope(object):
    def __init__(self,**kwargs):
        for name, val in kwargs.items():
            setattr(self,name,val)

    def __repr__(self):
        return str(Scope.__name__)+str(self.__dict__)

def PoolExecutor(parallel=1,dispatch=None,backend=None):
    parallel=calc_parallel(parallel)
    if backend is None or backend=='auto':
        if parallel>1:
            backend=futures.ProcessPoolExecutor
        elif parallel==1:
            backend=futures.ThreadPoolExecutor
        else:
            class BlockingSequentialExecutor(futures._base.Executor):
                def __init__(self,max_workers):
                    pass

                def submit(self, fn, *args, **kwargs):
                    f=futures.Future()
                    try:
                        res=fn(*args, **kwargs)
                    except Exception as e:
                        f.set_exception(e)
                    else:
                        f.set_result(res)
                    return f

            backend=BlockingSequentialExecutor

    elif isinstance(backend,str):
        backend_mapping={
            "process" : futures.ProcessPoolExecutor,
            "thread"  : futures.ThreadPoolExecutor,
        }
        backend= backend_mapping.get(backend,"process")

    class PoolExecutorAdapter(backend):
        def __init__(self,parallel=1,dispatch=None):
            parallel=calc_parallel(parallel)
            super().__init__(max_workers=parallel)

            self._num_dispatch=calc_dispatch(dispatch,parallel)
            self.running_items=set()

        def submit(self, fn, *args, **kwargs):
            if self._num_dispatch and len(self.running_items) >= self._num_dispatch:
                self.__wait()
            item=super().submit(fn, *args, **kwargs)
            if self._num_dispatch:
                self.running_items.add(item)
            return item

        def __wait(self):
            if len(self.running_items)==0:
                return set()
            done_items, self.running_items=futures.wait(self.running_items,return_when=futures.FIRST_COMPLETED)
            return done_items

        def __repr__(self):
            return "{super}(workers={workers}, dispatch={dispatch})".format(
                super=backend.__name__,
                workers=self._max_workers if hasattr(self,"_max_workers") else "ERR",
                dispatch=self._num_dispatch
            )

    return PoolExecutorAdapter(parallel,dispatch)

class QueueStopIndicator(object):
    def __init__(self,*args):
        self.reason=args
    def __eq__(self,operand):
        return isinstance(operand,QueueStopIndicator)
    def __repr__(self):
        if self.reason:
            return str(self.reason)
        else:
            return ""

class ThreadPipelineStage(threading.Thread):
    def __init__(self, inq=None, inq_stop=QueueStopIndicator(), out_qsize=0):
        super().__init__()
        self.inq=inq
        self.inq_stop=inq_stop
        self.outq=queue.Queue(maxsize=out_qsize)

    def run(self):
        try:
            if self.inq is None:
                self.f(*self.fargs,**self.fkwargs)
            else:
                for item in iter(self.inq.get,QueueStopIndicator()):
                    o=self.f(item,*self.fargs,**self.fkwargs)
                    self.outq.put(o)
        except Exception as e:
            self.outq.put(QueueStopIndicator({"from":self.name,"reason":e}))
        else:
            self.outq.put(QueueStopIndicator(self.name))

    def start(self):
        super().start()
        return self

    def target(self,target=lambda : None, *args,**kwargs):
        self.setName(target.__name__)
        self.f=target
        self.fargs=args
        self.fkwargs=kwargs
        return self

    def get_outq(self):
        return self.outq

def launchPipelineStage(func=lambda: None, *args,**kwargs):
    stage=ThreadPipelineStage().target(func,*args,**kwargs)
    stage.start()
    return stage.get_outq()