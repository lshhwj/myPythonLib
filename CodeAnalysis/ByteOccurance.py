from  Binary import Binary
import struct

class ByteOccurance_Engine(object):
    def __init__(self, filename=None):
        self.res=[0]*256
        self.size=0
        self.bin = None

        if filename:
            self.bin=Binary(filename)
            for sec in self.bin.getExecSectionsIter():
                self.size+=sec["size"]
                for b in sec["opcodes"]:
                    self.res[struct.unpack("=B",b)[0]]+=1


    def get_result(self):
        if not self.bin:
            raise Exception("No file to analyse!")
        return self.res

    def get_size(self):
        if not self.bin:
            raise Exception("No file to analyse!")
        return self.size


if __name__=="__main__":
    import sys

    eng=ByteOccurance_Engine(sys.argv[1])

    for i in range(256):
        print("{:02x} : {:#5} {:}%".format(i,eng.res[i],(float)(eng.res[i]*100)/eng.get_size()))

    
