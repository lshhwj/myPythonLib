import capstone as cap
from capstone.x86_const import *

X86_INS_JCC_union = X86_INS_ENDING + 1
X86_INS_NOP_followed= X86_INS_ENDING + 2

OtherNopInsts=frozenset([
    bytes(b"\x89\xf6"),
    bytes(b"\x8d\x74\x00"),
    bytes(b"\x8d\x74\x26\x00"),
    bytes(b"\x8d\x76\x00"),
    bytes(b"\x8d\xb4\x00\x00"),
    bytes(b"\x8d\xb4\x26\x00x\x00\x00\x00"),
    bytes(b"\x8d\xb6\x00\x00\x00\x00"),
    bytes(b"\x8d\xbd\x00\x00"),
    bytes(b"\x8d\xbc\x27\x00\x00\x00\x00"),
    bytes(b"\x8d\xbf\x00\x00\x00\x00"),
])

def IsOtherNopX86(ins_bytes):
    i=bytes(ins_bytes.lstrip(b"\x66"))
    return i in OtherNopInsts

"""

        // 8d 76 00 lea esi, [esi + 0]
        if (size==3 && m[1] == 0x76 && m[2] == 0x00) { return true; }
        if (m[1] == 0xb4) {
            // 8d b4 00 00 lea
            if (size==4 && m[2] == 0x00 && m[3] == 0x00) {return true;}
            // 8d b4 26 00 00 00 00 lea
            if (size==7 && m[2] == 0x26 && m[3] == 0x00 && m[4] == 0x00 && m[5] == 0x00 && m[6] == 0x00) {return true;}
        }
        // 8d b6 00 00 00 00 lea
        if (size==6 && m[1] == 0xb6 && m[2] == 0x00 && m[3] == 0x00 && m[4] == 0x00 && m[5] == 0x00) {return true;}
        // 8d bd 00 00 lea
        if (size==4 && m[1] == 0xbd && m[2] == 0x00 && m[3] == 0x00) {return true;}
        // 8d bc 27 00 00 00 00 lea
        if (size==7 && m[1] == 0xbc && m[2] == 0x27 && m[3] == 0x00 && m[4] == 0x00 && m[5] == 0x00 && m[6] == 0x00){return true;}
        // 8d bf 00 00 00 00 lea
        if (size==6 && m[1] == 0xbf && m[2] == 0x00 && m[3] == 0x00 && m[4] == 0x00 && m[5] == 0x00) {return true;}
    }
"""

class InstNameConverter(object):
    def __init__(self):
        self.__cs = cap.Cs(cap.CS_ARCH_X86, cap.CS_MODE_64)
        self.__cs.detail = True

        if self.__cs._diet:
            # Diet engine cannot provide instruction name
            raise cap.CsError(cap.CS_ERR_DIET)

    def getInstName(self,inst_id):
        if inst_id == 0:
            return "(invalid)"

        if inst_id == X86_INS_JCC_union:
            return "JCC"
        elif inst_id == X86_INS_NOP_followed:
            return "-NOP-"

        return cap._cs.cs_insn_name(self.__cs.csh, inst_id).decode('ascii')
