import subprocess as p
import re

class Finding_Engine(object):
    MODE_ALLINMEMORY=1
    MODE_PREPROCESSE2SET=2


    def __init__(self, filename=None, mode=MODE_ALLINMEMORY):
        if filename:
            self.file_path = filename
            self.mode = mode

            if mode == Finding_Engine.MODE_ALLINMEMORY:
                with open(filename, 'r') as f:
                    self.file_data = f.read()
            elif mode == Finding_Engine.MODE_PREPROCESSE2SET:
                self.addr_set=set()
                with open(filename, 'r') as f:
                    for l in f:
                        if re.match("^ +?[0-9a-f]+?:",l):
                            addr = int (l.split(":",1)[0],16)
                            self.addr_set.add(addr)
        else:
            self.file_path = ""
            self.file_data = ""

    def __contains__(self, addr_int):
        if self.mode ==Finding_Engine.MODE_ALLINMEMORY:
            pattern = " " + hex(addr_int)[2:] + ":\t"
            return pattern in self.file_data
        elif self.mode == Finding_Engine.MODE_PREPROCESSE2SET:
            return addr_int in self.addr_set


def PrintDistribution(gdt_pool,binaryfile=None,checkBB=False):
    cnt = {}
    eng=Finding_Engine(binaryfile)
    for addrtp in gdt_pool.iterkeys():
        if not (addrtp[1] in cnt.keys()):
            cnt.update({addrtp[1]: [0,0]})
        cnt[addrtp[1]][0] += 1
        if checkBB:
            cnt[addrtp[1]][1] += 1 if addrtp[0] in eng else 0
    print("NumInst\tNumGadget\tNumTrueBasicBlock")
    for NumInst, NumGadget in cnt.iteritems():
        print("{i:>7}\t{g:>9}\t{B:>17}".format(i=NumInst, g=NumGadget[0] ,B=NumGadget[1]))
    print("  Total\t{g:>9}\t{B:>17}".format(
        g=sum([x[0] for x in cnt.itervalues()]),
        B=sum([x[1] for x in cnt.itervalues()]))
    )


def find_addr(addr_int):
    cmd_basic="grep --silent -c -m 1 -e".split(" ")
    pattern = " " + hex(addr_int)[2:] + ":\t"
    #print pattern
    retcode = p.call( cmd_basic + [pattern ,"libc-2.19.so.asm"])
    if retcode>=2:
        raise p.CalledProcessError(retcode, "grep", None)
    return not retcode

if __name__=="__main__":
    a=find_addr(0x117407)
    print(str(type(a)))
    print(a)
    print(find_addr(0x117406))
