#!/usr/bin/env python3


# import sys
from optparse import OptionParser

def getargs():
    usage_string = "usage: %prog [options] bin_str"
    parser = OptionParser(usage=usage_string)

    parser.add_option("-i",
        action="store_true", dest="ignore_other", default=False,
        help="Set to ignore special characters between the 1/0 stream. Stop at 2-9a-z.")

    return parser.parse_args()

def get_int_concat(bin):
    clean_bin=""
    for c in bin:
        if c in ("1","0"):
            clean_bin+=c
        elif c.isalpha() or c.isdigit():
            break
    return clean_bin

if __name__ == "__main__":
    (options, args) = getargs()

    if options.ignore_other:
        bin=get_int_concat(args[0])
    else:
        bin=args[0]

    bin_int=int(bin,2)
    print( bin + " = " + hex(bin_int))