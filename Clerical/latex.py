
def __get_author_name(author_string):
    """
        >>> __get_author_name("Intel{\\textregistered}") == {'family': ['Intel{\\textregistered}'], 'given': [''] }
        True
        >>> __get_author_name("aaa, bbb-ccc ddd") == {'family': ['aaa'], 'given': ['bbb-ccc','ddd'] }
        True
    """
    family_names=author_string.split(',')[0].strip().split(' ')
    family_names=list(map(lambda x : x.strip(), family_names))
    if ',' in author_string:
        given_names=author_string.split(',')[1].strip().split(' ')
        given_names=list(map(lambda x : x.strip(), given_names))
    else:
        given_names=[]
    return {'family': family_names, 'given': given_names }

def __build_author_list(authors_string):
    """
        >>> __get_author_list("aaa, bbb and ccc, ddd and Intel{\\textregistered}") == \
            [{'family': ['aaa'], 'given': ['bbb'] }, \
            {'family': ['ccc'], 'given': ['ddd'] }, \
            {'family': ['Intel{\\textregistered}'], 'given': [] } \
            ]
        True
    """
    authors=authors_string.split('and ')
    authors=list(map(lambda x : x.strip(), authors))
    authors=list(map(__get_author_name, authors))
    return authors

def __author_list_abbr_given(authors:list):
    for idx, adict in enumerate(authors):
        new_givens=[]
        for given in adict["given"]:
            sub_givens=given.split("-")
            sub_givens=list(map(lambda x : x[0].upper()+".", sub_givens))
            new_sub="-".join(sub_givens)
            new_givens.append(new_sub)
        authors[idx]["given"]=new_givens
    return authors

def __author_list_to_string_list(authors:list):
    ret_list=[]
    for a in authors:
        s=" ".join(a["family"])
        if len(a["given"]):
            s+=", "
            s+=" ".join(a["given"])
        ret_list.append(s)
    return ret_list

def abbrev_author_list(authors_string):
    authors=__build_author_list(authors_string)
    authors=__author_list_abbr_given(authors)
    authors=__author_list_to_string_list(authors)
    return ' and '.join(authors)

def simple_process_bib(stream,perserve_author=False,exclude=[],commentify=[],commentify_prefix="xx_",indent="\t"):
    """ case-insensitive. only support properties within one line.
        for Advanced processing, we need bibtex parser.
    """
    exclude=[c.lower() for c in exclude]
    commentify=[c.lower() for c in commentify]
    for line in stream:
        if "=" not in line:
            yield line
            continue
        
        attr=line.split("=")[0].strip().lower()
        if attr in exclude:
            continue

        val=line.split("=")[1].strip()
        if attr == "author" and not perserve_author:
            original_author=indent + commentify_prefix + attr + " = " + val + "\n"
            val=val.rstrip(",").strip()[1:-1]
            val=abbrev_author_list(val)
            yield original_author + indent + attr + " = {" + val + "},"  + "\n"
        elif attr in commentify:
            yield indent + commentify_prefix + attr + " = " + val + "\n"
        else:
            yield line


if __name__=="__main__":
    import doctest
    (failure_count, test_count) = doctest.testmod()
    if not failure_count:
        print("[PASS] {} tests passed.".format(test_count))